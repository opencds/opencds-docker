In order to build this image, you must include the knowedlge repository and the opencds war file

- opencds-docker/files/opencds/opencds-decision-support-service.war
- opencds-docker/files/opencds/opencds-knowledge-repository-data.jar

Build image :
sudo docker build -t="opencds/opencds" .

Create volume for .opencds 
sudo docker create -v /root/.opencds --name dot-opencds opencds/opencds /bin/true

Create volume for webapps
sudo docker create -v /usr/local/tomcat/webapps --name opencds-webapps opencds/opencds /bin/true

Create container :
sudo docker run --volumes-from opencds-webapps --volumes-from dot-opencds -d -p 38080:8080 -p 38443:8443 --name opencds opencds/opencds

Opencds service will be available on 
- https://localhost:38443/opencds-decision-support-service/evaluate?wsdl	
or secure connection
- http://localhost:38080/opencds-decision-support-service/evaluate?wsdl	

Information about opencds service is available on http://www.opencds.org/

How to create self-signed certificate:
- refenrence http://tomcat.apache.org/tomcat-7.0-doc/ssl-howto.html
$JAVA_HOME/bin/keytool -genkey -alias tomcat -keyalg RSA -keystore ${CATALINA_HOME}

After runnig this command you will be promted for the key password.  By default the Dockerfile specify the password "opencds" on sever.xml file.


