# Version: 0.0.2

FROM ubuntu:14.04
MAINTAINER Salvador Rodriguez <salvador.rodriguez@utah.edu>

# Install packages
ENV REFRESHED_AT 2016-07-23
RUN apt-get update && \
    apt-get install -yq --no-install-recommends wget maven unzip tar pwgen ca-certificates && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

ENV NOTVISIBLE "in users profile"
RUN echo "export VISIBLE=now" >> /etc/profile

# Install Java
RUN apt-get update && \
  apt-get install -y software-properties-common && \
  add-apt-repository -y ppa:webupd8team/java && \
  (echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | /usr/bin/debconf-set-selections) && \
  apt-get update && \
  apt-get install -y oracle-java8-installer && \
  apt-get clean && \
  rm -fr /var/lib/apt/lists/* /tmp/* /var/tmp/*

ENV JAVA_HOME /usr/lib/jvm/java-8-oracle

# Set maven home
RUN echo export M2_HOME=/usr/share/maven >> ~/.bashrc

# Set maven settings
COPY files/maven/settings.xml /usr/share/maven/conf/

# Install TOMCAT
ENV TOMCAT_MAJOR_VERSION 9
ENV TOMCAT_MINOR_VERSION 9.0.0.M9
ENV CATALINA_HOME /usr/local/tomcat

RUN wget -q https://archive.apache.org/dist/tomcat/tomcat-${TOMCAT_MAJOR_VERSION}/v${TOMCAT_MINOR_VERSION}/bin/apache-tomcat-${TOMCAT_MINOR_VERSION}.tar.gz && \
    wget -qO- https://archive.apache.org/dist/tomcat/tomcat-${TOMCAT_MAJOR_VERSION}/v${TOMCAT_MINOR_VERSION}/bin/apache-tomcat-${TOMCAT_MINOR_VERSION}.tar.gz.md5 | md5sum -c - && \
    tar zxf apache-tomcat-*.tar.gz && \
    rm apache-tomcat-*.tar.gz && \
    mv apache-tomcat* ${CATALINA_HOME}

COPY files/tomcat/tomcat-users.xml ${CATALINA_HOME}/conf/tomcat-users.xml
COPY files/tomcat/setenv.sh ${CATALINA_HOME}/bin/setenv.sh
# Allow remote access to tomcat manager
COPY files/tomcat/manager.xml ${CATALINA_HOME}/conf/Catalina/localhost/manager.xml

# Add .keystore
COPY files/tomcat/.keystore ${CATALINA_HOME}/.keystore

# Configure https
RUN sed -i "s#</Server>##g" ${CATALINA_HOME}/conf/server.xml; \
	sed -i "s#  </Service>##g" ${CATALINA_HOME}/conf/server.xml; \
	echo '    <Connector port="8443" protocol="HTTP/1.1" SSLEnabled="true" maxThreads="150" scheme="https" secure="true" clientAuth="false" sslProtocol="TLS" keystoreFile=".keystore" keystorePass="opencds" />' >> ${CATALINA_HOME}/conf/server.xml; \
	echo '  </Service>' >> ${CATALINA_HOME}/conf/server.xml; \
	echo '</Server>' >> ${CATALINA_HOME}/conf/server.xml

# Install OpenCDS
COPY files/opencds/opencds.properties /root/.opencds/opencds.properties
COPY files/opencds/sec.xml /root/.opencds/sec.xml

# Copy knowledge repository
COPY files/opencds/opencds-knowledge-repository-data.jar / 
RUN unzip opencds-knowledge-repository-data.jar -d /root/.opencds/opencds-knowledge-repository-data/
RUN rm opencds-knowledge-repository-data.jar

# Copy opencds war
COPY files/opencds/opencds-decision-support-service.war /
RUN unzip opencds-decision-support-service.war -d ${CATALINA_HOME}/webapps/opencds-decision-support-service
RUN rm -f opencds-decision-support-service.war

# Add image scripts
COPY files/img_scripts/run.sh /usr/local/bin/run.sh
RUN chmod +x /usr/local/bin/run.sh

# Add VOLUMEs to allow backup of config and databases
VOLUME ["/root/.opencds", "/usr/local/tomcat/webapps"]

EXPOSE 8080
EXPOSE 8443
EXPOSE 1043

CMD ["/usr/local/bin/run.sh"]

